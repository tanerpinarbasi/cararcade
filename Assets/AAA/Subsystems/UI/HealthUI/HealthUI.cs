﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace CarArcade
{
	public class HealthUI : MonoBehaviour
	{
		[Required]
		public Image Image;

		[Required]
		public CarHealth CarHealth;

		void Update()
		{
			Image.fillAmount = (float)CarHealth.Health / CarHealth.MaxHealth;
		}
	}
}