﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CarArcade
{
	public class CarController : MonoBehaviour
	{
		#region Fields

		public float Acceleration = 10f;
		public float RotationForce = 10f;

		public Vector3 AccelerationForceOffset = new Vector3(0f, 0f, 0f);
		public Vector3 RotationForceOffset = new Vector3(0f, 0f, 2f);
		
		[Required]
		public Collider Collider;
		[Required]
		public Rigidbody Rigidbody;
		[Required]
		public DeviceID DeviceID;

		bool GasInput;
		bool HandbrakeInput;
		bool TurnLeft;
		bool TurnRight;
		Vector2 JoystickDirection;

		#endregion

		#region Input Setup

		public void SetupInput(GameInput input)
		{
			var player = input.Player;

			player.Gas.Enable();
			player.Gas.started += GasStarted;
			player.Gas.canceled += GasCancelled;

			player.Handbrake.Enable();
			player.Handbrake.started += HandbrakeStarted;
			player.Handbrake.canceled += HandbrakeCancelled;

			player.TurnLeft.Enable();
			player.TurnLeft.started += TurnLeftStarted;
			player.TurnLeft.canceled += TurnLeftCancelled;

			player.TurnRight.Enable();
			player.TurnRight.started += TurnRightStarted;
			player.TurnRight.canceled += TurnRightCancelled;

			player.Move.Enable();
			player.Move.performed += MovePerformed;
			player.Move.canceled += MoveCancelled;
		}

		#endregion

		#region Input Processing

		void HandbrakeCancelled(InputAction.CallbackContext context)
		{
			if(DeviceID.IsThisDevice(context))
				HandbrakeInput = false;
		}

		void HandbrakeStarted(InputAction.CallbackContext context)
		{
			if(DeviceID.IsThisDevice(context))
				HandbrakeInput = true;
		}

		void TurnRightCancelled(InputAction.CallbackContext context)
		{
			if(DeviceID.IsThisDevice(context))
				TurnRight = false;
		}

		void TurnRightStarted(InputAction.CallbackContext context)
		{
			if(DeviceID.IsThisDevice(context))
				TurnRight = true;
		}

		void TurnLeftCancelled(InputAction.CallbackContext context)
		{
			if(DeviceID.IsThisDevice(context))
				TurnLeft = false;
		}

		void TurnLeftStarted(InputAction.CallbackContext context)
		{
			if(DeviceID.IsThisDevice(context))
				TurnLeft = true;
		}

		void GasCancelled(InputAction.CallbackContext context)
		{
			if(DeviceID.IsThisDevice(context))
				GasInput = false;
		}

		void GasStarted(InputAction.CallbackContext context)
		{
			if(DeviceID.IsThisDevice(context))
				GasInput = true;
		}

		void MoveCancelled(InputAction.CallbackContext context)
		{
			JoystickDirection = Vector2.zero;
			TurnLeft = false;
			TurnRight = false;
		}
		
		void MovePerformed(InputAction.CallbackContext context)
		{
			if(DeviceID.IsThisDevice(context))
				JoystickDirection = context.ReadValue<Vector2>();
		}

		#endregion

		#region Update

		void FixedUpdate()
		{
			if(JoystickDirection != Vector2.zero)
			{
				var sensitivity = 0.1f;
				
				if(Mathf.Abs(JoystickDirection.x) > sensitivity || Mathf.Abs(JoystickDirection.y) > sensitivity)
				{
					var inputDirection = JoystickDirection.normalized;
					var joystickForward = new Vector2(0f, 1f);
					var angle = Vector2.SignedAngle(joystickForward, inputDirection);

					if(angle > 0f)
					{
						TurnLeft = true;
						TurnRight = false;
					}
					else
					{
						TurnRight = true;
						TurnLeft = false;
					}
				}
			}

			var centerOfMass = Rigidbody.worldCenterOfMass;
			var forward = transform.forward;
			var right = transform.right;
			var rightRotationForce = HandbrakeInput ? -right * RotationForce : right * RotationForce;
			var positiveAccelerationForce = forward * Acceleration;
			var accelerationOffset = transform.TransformDirection(AccelerationForceOffset);
			var rotationOffset = transform.TransformDirection(RotationForceOffset);
			var accelerationPosition = centerOfMass + accelerationOffset;
			var rotationPosition = centerOfMass + rotationOffset;
			
			if(GasInput)
			{
				Rigidbody.AddForceAtPosition(positiveAccelerationForce, accelerationPosition, ForceMode.Acceleration);
				Debug.DrawRay(accelerationPosition, positiveAccelerationForce, Color.yellow);
			}
			else if(HandbrakeInput)
			{
				Rigidbody.AddForceAtPosition(-positiveAccelerationForce, accelerationPosition, ForceMode.Acceleration);
				Debug.DrawRay(accelerationPosition, -positiveAccelerationForce, Color.yellow);
			}

			if(TurnLeft && (GasInput || HandbrakeInput))
			{
				Rigidbody.AddForceAtPosition(-rightRotationForce, rotationPosition, ForceMode.Acceleration);
				Debug.DrawRay(rotationPosition, -rightRotationForce, Color.red);
			}
			else if(TurnRight && (GasInput || HandbrakeInput)
			)
			{
				Rigidbody.AddForceAtPosition(rightRotationForce, rotationPosition, ForceMode.Acceleration);
				Debug.DrawRay(rotationPosition, rightRotationForce, Color.red);
			}
		}

		#endregion
	}
}