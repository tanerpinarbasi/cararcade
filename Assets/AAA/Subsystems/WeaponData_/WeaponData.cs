﻿using System.IO;
using UnityEngine;

namespace CarArcade
{
	[CreateAssetMenu]
	public class WeaponData : ScriptableObject
	{
		public string WeaponName;
		public float ProjectileSpeed;
		public float ProjectileRange;
		public int ProjectileDamage;
		public float FirePerSecond;
		public int AmmoCount;
		public int MagazineCapacity;
		public float ReloadDuration;
		public float Recoil;
		public Direction ProjectileDirection;
		public Projectile ProjectilePrefab;
		public AudioClip FireClip;
		public AudioClip ReloadClip;
		public AudioClip AmmoDepletedClip;
	}
}