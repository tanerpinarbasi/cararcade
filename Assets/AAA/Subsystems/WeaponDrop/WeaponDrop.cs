﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace CarArcade
{
	public class WeaponDrop : MonoBehaviour
	{
		[Required, AssetsOnly]
		public WeaponData WeaponData;

		void OnTriggerEnter(Collider other)
		{
			var fireController = other.GetComponent<CarFireController>();

			if(fireController != null)
			{
				fireController.PickUpWeapon(WeaponData);
				Destroy(gameObject);
			}
		}
	}
}