﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObstacle : MonoBehaviour
{
    // Start is called before the first frame update
    private Vector3 direction = Vector3.back;

    [SerializeField] private Transform pA;
    [SerializeField] private Transform pB;
    private Transform target;
    [SerializeField] private float speed;
    
    void Start()
    {
        target = pB;
    }

    // Update is called once per frame
    void Update()
    {
        //current transform = vector3.Movetowards(current pos , target?)
        //go to point b
        //if at point b
        //go to point a
        //if at point a
        //gp to point b
        
        if (gameObject.transform.position == pA.transform.position)
        {
            target = pB;
        }
        else if(gameObject.transform.position == pB.transform.position)
        {
            target = pA;
        }
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, target.position, speed * Time.deltaTime);
    }
}