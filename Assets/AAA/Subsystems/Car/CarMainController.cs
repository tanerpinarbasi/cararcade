﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace CarArcade
{
	public class CarMainController : MonoBehaviour
	{
		[AssetsOnly]
		public PhysicMaterial CarMaterial;
		
		[AssetsOnly]
		public GameObject CarModel;

		[Required, ChildGameObjectsOnly]
		public DeviceID DeviceID;

		[Required, ChildGameObjectsOnly]
		public CarController CarController;

		[Required, ChildGameObjectsOnly]
		public CarFireController FireController;

		void Awake()
		{
			var input = new GameInput();
			CarController.SetupInput(input);
			FireController.SetupInput(input);
		}

		void Start()
		{
			if(CarModel != null)
			{
				// Remove placeholder model and Collider
				{
					var placeholderModel = GetComponent<MeshRenderer>();
					Destroy(placeholderModel);
					//var baseCollider = GetComponent<Collider>();
					//Destroy(baseCollider);
				}

				// Create Car Model from Prefab
				{
					var model = Instantiate(CarModel, transform);

					var meshCollider = model.GetComponentInChildren<MeshCollider>();
					Destroy(meshCollider);
					//meshCollider.convex = true;
					//meshCollider.material = CarMaterial;
					
					model.transform.localPosition = Vector3.zero;
					model.transform.localRotation = Quaternion.identity;
					// Make model lossyScale 1
					var scale = transform.localScale;
					model.transform.localScale = new Vector3(1f / scale.x, 1f / scale.y, 1f / scale.z);
				}
			}
		}
	}
}