﻿using System.Collections.Generic;
using UnityEngine;

public class CarSpawnPosition : MonoBehaviour
{
	public static readonly List<Vector3> All = new List<Vector3>();

	Vector3 Pos;

	void OnEnable()
	{
		Pos = transform.position;
		All.Add(Pos);
	}

	void OnDisable()
	{
		All.Remove(Pos);
	}

	public static Vector3 UseRandom()
	{
		var point = Vector3.zero;
		
		if(All.Count > 0)
		{
			var i = Random.Range(0, All.Count);
			point = All[i];
			All.RemoveAt(i);
		}

		return point;
	}
}