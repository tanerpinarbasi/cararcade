using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CarArcade
{
	public enum Direction
	{
		North,
		South,
		West,
		East,
		NorthWest,
		NorthEast,
		SouthWest,
		SouthEast,
	}

	public class CarFireController : MonoBehaviour
	{
		#region Fields

		[Required, ChildGameObjectsOnly]
		public Transform NorthPosition;
		[Required, ChildGameObjectsOnly]
		public Transform SouthPosition;
		[Required, ChildGameObjectsOnly]
		public Transform WestPosition;
		[Required, ChildGameObjectsOnly]
		public Transform EastPosition;
		[Required, ChildGameObjectsOnly]
		public Transform NorthWestPosition;
		[Required, ChildGameObjectsOnly]
		public Transform NorthEastPosition;
		[Required, ChildGameObjectsOnly]
		public Transform SouthWestPosition;
		[Required, ChildGameObjectsOnly]
		public Transform SouthEastPosition;

		[Required, ChildGameObjectsOnly]
		public Rigidbody Rigidbody;
		
		[Required, ChildGameObjectsOnly]
		public DeviceID DeviceID;

		[Required, ChildGameObjectsOnly]
		public AudioSource AudioSource;

		float NextFireTime;

		WeaponData WeaponData;
		bool Fire;
		bool Reload;
		int FreeAmmo;
		int AmmoInMagazine;
		float ReloadEndTime;

		#endregion

		#region Input Setup

		public void SetupInput(GameInput input)
		{
			var player = input.Player;

			player.Fire.Enable();
			player.Fire.started += FireStarted;
			player.Fire.canceled += FireCancelled;
			
			player.Reload.Enable();
			player.Reload.started += ReloadStarted;
			player.Reload.canceled -= ReloadCancelled;
		}

		#endregion

		#region Input Processing
		
		void ReloadStarted(InputAction.CallbackContext context)
		{
			if(DeviceID.IsThisDevice(context))
				Reload = true;
		}

		void ReloadCancelled(InputAction.CallbackContext context)
		{
			if(DeviceID.IsThisDevice(context))
				Reload = false;
		}
		
		void FireStarted(InputAction.CallbackContext context)
		{
			if(DeviceID.IsThisDevice(context))
				Fire = true;
		}

		void FireCancelled(InputAction.CallbackContext context)
		{
			if(DeviceID.IsThisDevice(context))
				Fire = false;
		}

		#endregion

		#region Weapon

		public void PickUpWeapon(WeaponData weaponData)
		{
			WeaponData = weaponData;
			AmmoInMagazine = weaponData.MagazineCapacity;
			FreeAmmo = weaponData.AmmoCount - AmmoInMagazine;
		}

		bool IsReloading()
		{
			return Time.time < ReloadEndTime;
		}

		bool IsEquippedWeapon()
		{
			return WeaponData != null;
		}

		bool IsAmmoDepleted()
		{
			return AmmoInMagazine == 0 && FreeAmmo == 0;
		}

		bool IsMagazineFull()
		{
			return AmmoInMagazine == WeaponData.MagazineCapacity;
		}
		
		bool IsMagazineEmpty()
		{
			return AmmoInMagazine == 0;
		}

		bool IsFireOutOfCooldown()
		{
			return Time.time > NextFireTime;
		}

		void BeginReload()
		{
			PlayReloadClip();
			
			ReloadEndTime = Time.time + WeaponData.ReloadDuration;
			var ammoToAdd = FreeAmmo > WeaponData.MagazineCapacity ? WeaponData.MagazineCapacity : FreeAmmo;
			FreeAmmo -= ammoToAdd;
			AmmoInMagazine += ammoToAdd;
		}

		void UseAmmo()
		{
			AmmoInMagazine--;
		}

		float GetNextFireTime()
		{
			return Time.time + 1f / WeaponData.FirePerSecond;
		}

		(Vector3, Vector3) GetProjectilePositionAndDirection()
		{
			switch(WeaponData.ProjectileDirection)
			{
				case Direction.North:
					return NorthPosition != null ? (NorthPosition.position, NorthPosition.forward) : (transform.position, transform.forward);
				case Direction.South:
					return SouthPosition != null ? (SouthPosition.position, SouthPosition.forward) : (transform.position, transform.forward);
				case Direction.West:
					return WestPosition != null ? (WestPosition.position, WestPosition.forward) : (transform.position, transform.forward);
				case Direction.East:
					return EastPosition != null ? (EastPosition.position, EastPosition.forward) : (transform.position, transform.forward);
				case Direction.NorthWest:
					return NorthWestPosition != null ? (NorthWestPosition.position, NorthWestPosition.forward) : (transform.position, transform.forward);
				case Direction.NorthEast:
					return NorthEastPosition != null ? (NorthEastPosition.position, NorthEastPosition.forward) : (transform.position, transform.forward);
				case Direction.SouthWest:
					return SouthWestPosition != null ? (SouthWestPosition.position, SouthWestPosition.forward) : (transform.position, transform.forward);
				case Direction.SouthEast:
					return SouthEastPosition != null ? (SouthEastPosition.position, SouthEastPosition.forward) : (transform.position, transform.forward);
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		void SpawnProjectile()
		{
			var positionAndDirection = GetProjectilePositionAndDirection();
			var position = positionAndDirection.Item1;
			var direction = positionAndDirection.Item2;
			
			var projectile = Instantiate(WeaponData.ProjectilePrefab, position, Quaternion.identity);
			projectile.Init(gameObject, direction, WeaponData.ProjectileDamage, WeaponData.ProjectileSpeed, WeaponData.ProjectileRange);
		}

		void ApplyRecoilPushback()
		{
			var centerOfMass = Rigidbody.centerOfMass;
			var backward = -transform.forward;
			Rigidbody.AddForceAtPosition(backward * WeaponData.Recoil, centerOfMass, ForceMode.Impulse);
		}

		void PlayReloadClip()
		{
			AudioSource.PlayOneShot(WeaponData.ReloadClip);
		}

		void PlayFireClip()
		{
			AudioSource.PlayOneShot(WeaponData.FireClip);
		}

		void PlayAmmoDepletedClip()
		{
			AudioSource.PlayOneShot(WeaponData.AmmoDepletedClip);
		}

		#endregion

		#region Update

		void Update()
		{		
			// if(Fire)
			// {
			// 	Debug.Log(Fire);
			// 	Debug.Log(IsEquippedWeapon());
			// 	Debug.Log(IsFireOutOfCooldown());
			// 	Debug.Log(!IsAmmoDepleted());
			// 	Debug.Log(!IsReloading());
			// }

			if(
				Reload &&
				!IsReloading() &&
				!IsAmmoDepleted() && 
				!IsMagazineFull())
			{
				BeginReload();
				Reload = false;
			}
			
			if(
				Fire &&
				IsEquippedWeapon() && 
				IsFireOutOfCooldown() && 
				!IsReloading())
			{
				if(IsMagazineEmpty())
				{
					PlayAmmoDepletedClip();
				}
				else
				{
					// Fire
					UseAmmo();
					SpawnProjectile();
					PlayFireClip();
					ApplyRecoilPushback();
				}
				
                NextFireTime = GetNextFireTime();
			}
		}

		#endregion
	}
}