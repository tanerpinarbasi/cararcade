﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

namespace CarArcade
{
	public static class Util
	{
		public static T GetRandom<T>(this List<T> list)
		{
			return list[Random.Range(0, list.Count)];
		}
	}

	public class GameManager : MonoBehaviour
	{
		[Required]
		public CarMainController ControllerPrefab;

		void OnEnable()
		{
			InputSystem.onDeviceChange += OnDeviceChanged;
		}

		void OnDisable()
		{
			InputSystem.onDeviceChange -= OnDeviceChanged;
		}

		void Start()
		{
			var allGamepads = Gamepad.all;
			var keyboard = Keyboard.current;
			
			Debug.Log("Connected Gamepads: " + allGamepads.Count);
			
			if(keyboard != null)
			{
				Debug.Log("Connected Keyboard");
				CreatePlayer(keyboard.deviceId);
			}

			foreach(var gamepad in allGamepads)
			{
				CreatePlayer(gamepad.deviceId);
			}
		}

		void CreatePlayer(int deviceId)
		{
			var playerController = Instantiate(ControllerPrefab);
			playerController.transform.position = CarSpawnPosition.UseRandom();
			playerController.DeviceID.SetDeviceID(deviceId);
		}

		void OnDeviceChanged(InputDevice device, InputDeviceChange change)
		{
			Debug.Log($"OnDeviceChanged\n{device.description}\n{change}");

			switch(change)
			{
				case InputDeviceChange.Added:
				{
					CreatePlayer(device.deviceId);
					break;
				}
				case InputDeviceChange.Removed:
					break;
				case InputDeviceChange.Disconnected:
					break;
				case InputDeviceChange.Reconnected:
					break;
				case InputDeviceChange.Enabled:
					break;
				case InputDeviceChange.Disabled:
					break;
				case InputDeviceChange.UsageChanged:
					break;
				case InputDeviceChange.ConfigurationChanged:
					break;
				case InputDeviceChange.Destroyed:
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(change), change, null);
			}
		}
	}
}