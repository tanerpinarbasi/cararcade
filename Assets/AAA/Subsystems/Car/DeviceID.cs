﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace CarArcade
{
	public class DeviceID : MonoBehaviour
	{
		int ID = -1;

		public void SetDeviceID(int id)
		{
			ID = id;
		}

		public bool IsThisDevice(InputAction.CallbackContext context)
		{
			if(ID == -1)
				return true; // If ID wasn't set DeviceManager should be missing so let all players control this.
			
			return ID == context.control.device.deviceId;
		}
	}
}