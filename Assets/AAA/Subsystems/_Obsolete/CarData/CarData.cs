﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace CarArcade
{
	[CreateAssetMenu]
	public class CarData : ScriptableObject
	{
		public float Acceleration = 5f;
		public float MaxSpeed = 5f;
		public float RotationSpeed = 5f;
		public int MaxHealth = 100;

		[PreviewField(75, ObjectFieldAlignment.Left)]
		public GameObject CarModel;

		[Required]
		public WeaponData WeaponData;
	}
}