﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class EditorDevelopment : MonoBehaviour
{
	[MenuItem("CarArcade/FixNonConvexMesh")]
	static void FixNonConvexMesh()
	{
		AssetDatabase.Refresh();

		foreach(var path in AssetDatabase.FindAssets("t:Object")
		                                 .Select(AssetDatabase.GUIDToAssetPath)
		                                 .Where(path => path.EndsWith(".prefab", StringComparison.Ordinal)))
		{
			var asset = AssetDatabase.LoadAssetAtPath<GameObject>(path);
			var meshCollider = asset.GetComponent<MeshCollider>();

			if(meshCollider != null && !meshCollider.convex)
			{
				meshCollider.convex = true;
				EditorUtility.SetDirty(asset);
			}
		}

		AssetDatabase.SaveAssets();
	}
}