﻿using CarArcade;
using Sirenix.OdinInspector;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [Required]
    public Rigidbody Rigidbody;
    
    GameObject Owner;
    Vector3 InitialPosition;
    int Damage;
    float Range;

    public void Init(GameObject owner, Vector3 direction, int damage, float speed, float range)
    {
        Owner = owner;
        Damage = damage;
        Rigidbody.velocity = direction.normalized * speed;
        InitialPosition = transform.position;
        Range = range;
    }

    void Update()
    {
        if(Vector3.Distance(InitialPosition, Rigidbody.position) > Range)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject != Owner)
        {
            var carHealth = other.GetComponentInParent<CarHealth>();

            if(carHealth != null)
            {
                carHealth.TakeDamage(Damage);
                Destroy(gameObject);
            }
        }
    }
}
