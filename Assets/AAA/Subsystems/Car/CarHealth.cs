﻿using UnityEngine;

namespace CarArcade
{
	public class CarHealth : MonoBehaviour
	{
		public int MaxHealth;
		public int Health { get; private set; }

		void Start()
		{
			Health = MaxHealth;
		}
		
		public void TakeDamage(int damage)
		{
			Health -= damage;

			if(Health <= 0f)
				Destroy(gameObject);
		}
	}
}